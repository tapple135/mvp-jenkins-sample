# mvp-jenkins-sample  
jenkins 빌드 / helm chart 배포 


## 1.  gitlab clone
  - https://gitlab.com/hyukseoung/mvp-jenkins-sample.git

## 2. Spring Tool Suite에 import
  - import > git > project from git > Clone URI (복사한 gitlab 주소 넣고 확인)
  - 프로젝트 deployment 폴더의 pipeline-icp-properties 파일 수정(붉은색 박스 부분만 수정)
     - namespace -> ICP에 배포할 namespace
	 - image -> docker image 저장소
	 - helmChartname -> ICP catalog에 보여질 이름
	 - releaseName -> ICP 배포명
	 - chartPath -> 프로젝트 폴더구조와 동일(Chart.yaml 파일까지 경로)
	 - git commit  
	 - ![image](./images/2019-10-21 22_25_01-시작.png)

## 3. jenkins pipeline 생성
  - http://169.56.164.51:30821/ 접속
  - New Item 생성(아래 이미지 참조)
  - ![image](./images/2019-10-21 22_33_45-시작.png)
  - git repository 정보 입력(아래 이미지 참조)
  - ![image](./images/2019-10-21 22_37_06-시작.png)

## 4. 배포 테스트 
  - Build now 실행
  - 아래 url 화면이 정상적으로 뜨는지 확인
  - http://dashboard1.169.56.164.52.nip.io/swagger-ui.html
  - ![image](./images/2019-10-21 22_43_02-시작.png)


	 